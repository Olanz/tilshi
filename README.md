# Tilshi

This is a small project designed for the creation of an automatic translation service from English to Kazak and vice versa
based on neural networks.

The main prerequisites are:
- Python
- Node.js
- Telegram bot API
- Tensorflow

The system will be based on NLP neural networks core that would provide automatic translation
and will be hosted on Google Cloud.

The 2 main interfaces to this NLP core available to users are:
- Telegram bot
- Website

Possible clients would be any user that has an access to smartphone or PC.
